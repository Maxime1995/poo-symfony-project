<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(
    name: 'first-user-admin',
    description: 'Create an admin user',
)]
class FirstUserAdminCommand extends Command
{
    private EntityManagerInterface $entityManager;
    private UserPasswordHasherInterface $passwordHasher;
    public function __construct(EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordHasher)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->passwordHasher = $passwordHasher;
    }
    protected function configure(): void
    {
      $this
        ->setDescription('Commande permettant la création d\'un utilisateur admin s\'il n\'en existe aucun')
        ->addArgument('username', InputArgument::REQUIRED, 'Admin username')
        ->addArgument('email', InputArgument::REQUIRED, 'Admin email')
        ->addArgument('password', InputArgument::REQUIRED, 'Admin password');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
      $username = $input->getArgument('username');
      $email = $input->getArgument('email');
      $password = $input->getArgument('password');

      // Vérifie si un utilisateur admin existe déjà
      $userRepository = $this->entityManager->getRepository(User::class);
      $adminUser = $userRepository->findOneBy(['role' => 'ROLE_ADMIN']);

      if (!$adminUser) {
        // Crée un nouvel utilisateur admin
        $adminUser = new User();
        $adminUser->setUsername($username);
        $adminUser->setPassword($this->passwordHasher->hashPassword($adminUser, $password));
        $adminUser->setEmail($email);
        $adminUser->setCreated(new \DateTime("now"));
        $adminUser->setRole('ROLE_ADMIN');

        $this->entityManager->persist($adminUser);
        $this->entityManager->flush();

        $output->writeln('L\'utilisateur admin a été créé.');
      } else {
        $output->writeln('Un utilisateur admin existe déjà.');
      }

        return Command::SUCCESS;
    }
}
