<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AuthorizationController extends AbstractController
{
  private AuthorizationCheckerInterface $authorizationChecker;

  public function __construct(AuthorizationCheckerInterface $authorizationChecker)
  {
    $this->authorizationChecker = $authorizationChecker;
  }

  /**
   * @Route("/check-auth", name="check_auth")
   */
  public function checkAuthentication(): Response
  {
    if ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
      // L'utilisateur est connecté
      return $this->render('authorization/authorized.html.twig');
    } else {
      // L'utilisateur n'est pas connecté
      return $this->render('authorization/unauthorized.html.twig');
    }
  }
}
