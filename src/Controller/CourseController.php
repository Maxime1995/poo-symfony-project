<?php

namespace App\Controller;

use App\Repository\CourseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CourseController extends AbstractController
{
    #[Route('/course', name: 'courseList')]
    public function list(CourseRepository $courseRepository): Response
    {
      $courses = $courseRepository->coursesFindAll();

      return $this->render('course/course.html.twig', ['courses' => $courses]);
    }
}
