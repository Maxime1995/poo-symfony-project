<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RoleFormType;
use App\Form\UserFormType;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({UserListener::class})
 */
class UserController extends AbstractController
{
  public function __construct(private ManagerRegistry $doctrine) {
  }

  #[
    Route('/user/list', name: 'userList', methods: 'GET'),
    IsGranted(new Expression('is_granted("ROLE_ADMIN")'))
  ]
  public function list(UserRepository $userRepository): Response
  {
    $users = $userRepository->usersFindAll();

    return $this->render('userList/userList.html.twig', ['users' => $users]);
  }

  #[
    Route('/user/{id}', name: 'userProfile', methods: 'GET'),
    IsGranted(new Expression('is_granted("ROLE_ADMIN") or is_granted("ROLE_USER")'))
  ]
  public function getUserById(User $user): Response
  {
    $user = $this->doctrine->getRepository(User::class)->findOneBy(['id' => $user->getId()]);
    // Vérifier si l'utilisateur existe
    if (!$user) {
      throw $this->createNotFoundException('Utilisateur non trouvé');
    }
    return $this->render('userProfile/userProfile.html.twig', ['profile' => $user]);
  }

  #[
    Route('/user/{id}/edit', name: 'userUpdate'),
    IsGranted(new Expression('is_granted("IS_AUTHENTICATED_FULLY")'))
  ]
  public function editUser(Request $request, UserPasswordHasherInterface $passwordHasher, ManagerRegistry $doctrine, User $user): Response
  {
    $userToEdit = $this->doctrine->getRepository(User::class)->findOneBy(['id' => $user->getId()]);

    $form = $this->createForm(UserFormType::class, $userToEdit);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $newPassword = $form->get('password')->getData();
      $email = $form->get('email')->getData();

      if ($newPassword) {
        $encodedPassword = $passwordHasher->hashPassword($user, $newPassword);
        $user->setPassword($encodedPassword);
      }

      if ($email) {
        $user->setEmail($email);
      }

      $entityManager = $doctrine->getManager();
      $entityManager->persist($user);
      $entityManager->flush();

      $this->addFlash('success', 'Utilisateur modifié avec succès');

      return $this->redirectToRoute('userProfile', ['id' => $user->getId()]);
    }

    return $this->render('userUpdate/userUpdate.html.twig', [
      'form' => $form->createView(),
    ]);
  }

  #[
    Route('/user/{id}/export', name: 'userJsonExport'),
    IsGranted(new Expression('is_granted("IS_AUTHENTICATED_FULLY")'))
  ]
  public function exportToJson(): Response
  {
    // Récupére les données de l'utilisateur
    $user = $this->getUser();
    $userCreated = $user->getCreated();
    $userLastLogin = $user->getlastLogin();

    $createdDate = $userCreated->format('d-m-Y H:i:s');
    $lastLoginDate = $userLastLogin->format('d-m-Y H:i:s');

    $data = [
      'username' => $user->getUsername(),
      'email'=> $user->getEmail(),
      'created' => $createdDate,
      'lastLogin' => $lastLoginDate,
      'role' => $user->getRoles()
    ];

    $jsonContent = json_encode($data);
    $response = new Response($jsonContent);
    $filename = $user->getUsername() . '_' . time() . '.json';
    // Ajoute des en-têtes pour spécifier le type de la réponse
    $response->headers->set('Content-Type', 'application/json');
    $response->headers->set('Content-disposition', 'attachment; filename="' . $filename . '"');

    $this->addFlash('success', 'Vos données ont bien été exportées au format JSON');

    return $response;
  }

  #[
    Route('/user/{id}/role/edit', name: 'editUserRole'),
    IsGranted(new Expression('is_granted("ROLE_ADMIN")'))
  ]
  public function editUserRole(Request $request, ManagerRegistry $doctrine, User $user): Response
  {
    $userToEdit = $this->doctrine->getRepository(User::class)->findOneBy(['id' => $user->getId()]);
    $username = $userToEdit->getUsername();

    $form = $this->createForm(RoleFormType::class);
    $form->handleRequest($request);

    $selectedRole = $form->get('role')->getData();

    if ($selectedRole) {
      $userToEdit->setRole($selectedRole);
      $entityManager = $doctrine->getManager();
      $entityManager->persist($userToEdit);
      $entityManager->flush();

      $this->addFlash('success', 'Rôle utilisateur modifié avec succès');

      return $this->redirectToRoute('userList');
    }
    return $this->render('userRoleUpdate/userRoleUpdate.html.twig', [
      'form' => $form,
      'username' => $username
    ]);

  }
}