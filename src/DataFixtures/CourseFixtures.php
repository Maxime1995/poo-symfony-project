<?php

namespace App\DataFixtures;

use App\Entity\Course;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CourseFixtures extends Fixture
{
  public function load(ObjectManager $manager) {
    $data = array(
      array(1, 'Base des réseaux', '1351'),
      array(2, 'Environnement et technologies du web', '1352'),
      array(3, 'SGBD (Système de gestion de bases de données)', '1353'),
      array(4, 'Création de sites web statiques', '1354'),
      array(5, 'Approche Design', '1355'),
      array(6, 'CMS - niveau 1', '1356'),
      array(7, 'Initiation à la programmation', '1357'),
      array(8, 'Activités professionnelles de formation', '1358'),
      array(9, 'Scripts clients', '1359'),
      array(10, 'Scripts serveurs', '1360'),
      array(11, 'Framework et POO côté Serveur', '1361'),
      array(12, 'Projet Web dynamique', '1362'),
      array(13, 'Veille technologique', '1363'),
      array(14, 'Epreuve intégrée', '1364'),
      array(15, 'Anglais UE1', '1783'),
      array(16, 'Anglais UE2', '1784'),
      array(17, 'Initiation aux bases de données', '1440'),
      array(18, 'Principes algorithmiques et programmation', '1442'),
      array(19, 'Programmation orientée objet', '1443'),
      array(20, 'Web : principes de base', '1444'),
      array(21, 'Techniques de gestion de projet', '1448'),
      array(22, 'Principes d’analyse informatique', '1449'),
      array(23, 'Eléments de statistique', '1755'),
      array(24, 'Structure des ordinateurs', '1808'),
      array(25, 'Gestion et exploitation de bases de données', '1811'),
      array(26, 'Mathématiques appliquées à l’informatique', '1807'),
      array(27, 'Bases des réseaux', '1323'),
      array(28, 'Projet d’analyse et de conception', '1450'),
      array(29, 'Information et communication professionnelle', '1754'),
      array(30, 'Produits logiciels de gestion intégrés', '1438'),
      array(31, 'Administration, gestion et sécurisation des réseaux', '1439'),
      array(32, 'Projet de développement SGBD', '1446'),
      array(33, 'Stage d’intégration professionnelle', '1451'),
      array(34, 'Projet d’intégration de développement', '1447'),
      array(35, 'Activités professionnelles de formation', '1452'),
      array(36, 'Epreuve intégrée de la section', '1453')
    );

    foreach ($data as $row) {
      $course = new Course();
      $course->setName($row[1]);
      $course->setCode($row[2]);
      $manager->persist($course);
    }

    $manager->flush();


  }
}