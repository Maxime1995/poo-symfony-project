<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class RoleFormType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('role', ChoiceType::class, [
        'choices' => [
          'Visiteur' => 'ROLE_USER',
          'Etudiant' => 'ROLE_STUDENT',
          'Professeur' => 'ROLE_PROF'
        ],
        'placeholder' => 'Sélectionnez une option',
        'required' => true,
      ])
      ->add('save', SubmitType::class, ['label' => 'Changer de role']);
  }
}