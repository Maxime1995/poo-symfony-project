<?php

namespace App\Security;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\SecurityRequestAttributes;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class UserAuthenticator extends AbstractLoginFormAuthenticator
{
    use TargetPathTrait;

    private EntityManagerInterface $entityManager;
    private SessionInterface $session;
    public const LOGIN_ROUTE = 'app_login';

  public function __construct(private UrlGeneratorInterface $urlGenerator, SessionInterface $session, EntityManagerInterface $entityManager)
    {
      $this->session = $session;
      $this->entityManager = $entityManager;
    }

    public function authenticate(Request $request): Passport
    {
        $username = $request->request->get('username', '');

        $request->getSession()->set(SecurityRequestAttributes::LAST_USERNAME, $username);

        return new Passport(
            new UserBadge($username),
            new PasswordCredentials($request->request->get('password', '')),
            [
                new CsrfTokenBadge('authenticate', $request->request->get('_csrf_token')),
                new RememberMeBadge(),
            ],
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
          if ($targetPath = $this->getTargetPath($request->getSession(), $firewallName)) {
              return new RedirectResponse($targetPath);
          }

          $user = $token->getUser();

        // Vérifie si l'utilisateur est une instance de UserInterface
        if ($user instanceof UserInterface) {
          $username = $user->getUsername();
          $user->setLastLogin(new \DateTime("now"));
          $this->entityManager->persist($user);
          $this->entityManager->flush();
          // Ajoutez un message flash avec le nom d'utilisateur
          $this->session->getFlashBag()->add('success', 'Authentification réussie ! Bienvenue ' . $username);
          return new RedirectResponse($this->urlGenerator->generate('userProfile', ['id' => $user->getId()]));
        }
        $this->session->getFlashBag()->add('danger', 'L\'Authentification a échouée !');
        return new RedirectResponse($this->urlGenerator->generate('app_home'));
    }

    protected function getLoginUrl(Request $request): string
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }

}
